require 'nn'

model = nn.Sequential()
model:add(nn.Linear(2, 32))
model:add(nn.ReLU())
model:add(nn.Linear(32, 1))
model:add(nn.Sigmoid())

criterion = nn.MSECriterion()

function batchMaker(batchSize)
  local batchInputs = torch.DoubleTensor(batchSize, 2)
  local batchLabels = torch.DoubleTensor(batchSize)
  for i = 1, batchSize do
    local input = torch.randn(2)
    local label
    if input[1] * input[2] > 0 then
      label = 0
    else
      label = 1
    end
    batchInputs[i]:copy(input)
    batchLabels[i] = label
  end
  return batchInputs, batchLabels
end

params, gradParams = model:getParameters()

local optimState = {learningRate = 0.01}

require 'optim'

for epoch = 1, 5000 do
  print('epoch #' .. epoch)
  function feval(params)
    gradParams:zero()
    
    local batchInputs, batchLabels = batchMaker(32)
    
    local outputs = model:forward(batchInputs)
    local loss = criterion:forward(outputs, batchLabels)
    local dloss_doutputs = criterion:backward(outputs, batchLabels)
    model:backward(batchInputs, dloss_doutputs)

    return loss, gradParams
  end
  optim.sgd(feval, params, optimState)
end

require 'image'
s = 6
m = 200
img = torch.Tensor(3, m, m)
for i = 1, m do
  for j = 1, m do
    local x = torch.Tensor(2)
    x[1] = s * (i/m-0.5)
    x[2] = s * (j/m-0.5)
    local y = model:forward(x)
    y = torch.sign(y-0.5)
    for k = 1, 3 do
      img[k][i][j] = y
    end
  end
end
img = img/2 + 0.5
image.save('result.png', img)
print('Image exported')
