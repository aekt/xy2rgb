require 'nn'
require 'image'
img = image.lena()
--img = image.scale(img, 64, 64)
h = img:size(2)
w = img:size(3)
meanXY = torch.Tensor({(1+h)/2, (1+w)/2})
meanRGB = torch.squeeze(torch.mean(torch.mean(img, 2), 3))
print('# preparing training data')
n = h * w
dataset = {}
function dataset:size() return n end
for i = 1, h do
  for j = 1, w do
    local XY = torch.Tensor(2)
    local RGB = torch.Tensor(3)
    XY[1] = i
    XY[2] = j
    for k = 1, 3 do
      RGB[k] = img[k][i][j]
    end
    XY = XY - meanXY
    --RGB = RGB - meanRGB
    dataset[(i-1)*w+j] = {XY, RGB}
  end
end
print('# defining network structure')
mlp = nn.Sequential()
mlp:add(nn.Linear(2, 16))
mlp:add(nn.ReLU())
mlp:add(nn.Linear(16, 32))
mlp:add(nn.ReLU())
mlp:add(nn.Linear(32, 64))
mlp:add(nn.ReLU())
mlp:add(nn.Linear(64, 3))
mlp:add(nn.Sigmoid())
print('# building test function')
function query(model)
  local m = 256
  local timg = torch.Tensor(3, m, m)
  for i = 1, m do
    for j = 1, m do
      local XY = torch.Tensor(2)
      XY[1] = i/m*h
      XY[2] = j/m*h
      XY = XY - meanXY
      local RGB = model:forward(XY)
      --RGB = RGB + meanRGB
      for k = 1, 3 do
        timg[k][i][j] = RGB[k]
      end
    end
  end
  return timg
end
print('# training network')
criterion = nn.MSECriterion()
trainer = nn.StochasticGradient(mlp, criterion)
trainer.learningRate = 0.01
trainer.maxIteration = 1000
function trainer:hookIteration(it)
  if it % 25 == 0 then
    print('# epoch ' .. it)
    if it % 50 == 0 then
      timg = query(self.module)
      image.save('t' .. it .. '.png', timg)
    end
  end
end
trainer:train(dataset)
print('# testing')
image.save('test.png', query(mlp))
print('# done')

