require 'image'
require 'nn'
-- prepare dataset
n = 100
dataset = {}
function dataset:size() return n end
for i = 1, n do
  local input = torch.randn(2);
  local output = torch.Tensor(1);
  if input[1] * input[2] > 0 then
    output[1] = -1;
  else
    output[1] = 1;
  end
  dataset[i] = {input, output}
end
-- define network
mlp = nn.Sequential()
mlp:add(nn.Linear(2, 4))
mlp:add(nn.ReLU())
mlp:add(nn.Linear(4, 4))
mlp:add(nn.ReLU())
mlp:add(nn.Linear(4, 1))
-- setup trainer
criterion = nn.MSECriterion()
trainer = nn.StochasticGradient(mlp, criterion)
trainer.learningRate = 0.01
trainer.maxIteration = 1000
trainer.verbose = false
print('Training ...')
trainer:train(dataset)
print('Exporting ...')
-- generate result image
s = 6
m = 256
x = torch.Tensor(2)
img = torch.Tensor(3, m, m)
for i = 1, m do
  for j = 1, m do
    x[1] = s * (i/m-0.5)
    x[2] = s * (j/m-0.5)
    y = mlp:forward(x)
    for k = 1, 3 do
      img[k][i][j] = y
    end
  end
end
img = img / torch.max(torch.abs(img))
img = img / 2 + 0.5
-- show training data
for i = 1, n do
  local tx = dataset[i][1][1]
  local ty = dataset[i][1][2]
  local function nz(a) return m*(a/s+0.5) end
  tx = nz(tx)
  ty = nz(ty)
  local color
  if dataset[i][2][1] > 0 then
    color = {0, 0, 255}
  else
    color = {255, 0, 0}
  end
  img = image.drawRect(img, tx, ty, tx, ty, {lineWidth = 5, color=color})
end
image.save('result.png', img)
print('Done.')

