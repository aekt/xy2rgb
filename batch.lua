require 'image'
function check(model, resolution)
  local m = resolution
  local timg = torch.Tensor(3, m, m)
  for i = 1, m do
    for j = 1, m do
      local x = i/m
      local y = j/m
      local XY = torch.Tensor({
        x, y, x^2, y^2, x*y,
        math.cos(x), math.sin(x),
        math.cos(y), math.sin(y)})
      local RGB = mlp:forward(XY)
      for k = 1, 3 do
        timg[k][i][j] = RGB[k]
      end
    end
  end
  return timg
end
print('# preparing training data')
img = image.lena()
img = image.scale(img, 128, 128)
h = img:size(2)
w = img:size(3)
n = h * w
batchInputs = torch.Tensor(n, 9)
batchLabels = torch.Tensor(n, 3)
for i = 1, h do
  for j = 1, w do
    local x = i/h
    local y = j/w
    local XY = torch.Tensor({
      x, y, x^2, y^2, x*y,
      math.cos(x), math.sin(x),
      math.cos(y), math.sin(y)})
    local RGB = torch.Tensor(3)
    for k = 1, 3 do
      RGB[k] = img[k][i][j]
    end
    batchInputs[(i-1)*w+j]:copy(XY)
    batchLabels[(i-1)*w+j]:copy(RGB)
  end
end
require 'nn'
mlp = nn.Sequential()
mlp:add(nn.Linear(9, 100))
mlp:add(nn.ReLU())
mlp:add(nn.Linear(100, 100))
mlp:add(nn.ReLU())
mlp:add(nn.Linear(100, 3))
mlp:add(nn.Sigmoid())
criterion = nn.MSECriterion()
params, gradParams = mlp:getParameters()
require 'optim'
optimState = {learningRate = 0.01}
for epoch = 1, 5000 do
  function feval(params)
    gradParams:zero()
    local outputs = mlp:forward(batchInputs)
    local loss = criterion:forward(outputs, batchLabels)
    local graddy = criterion:backward(outputs, batchLabels)
    mlp:backward(batchInputs, graddy)
    print('# loss: ' .. loss)
    return loss, gradParams
  end
  optim.adam(feval, params, optimState)
  if epoch % 10 == 0 then
    print('epoch #' .. epoch)
    image.save('tmp.png', check(mlp, 256))
  end
end
image.save('result.png', check(mlp, 512))

