require 'nn'
hs = 5

block1 = nn.Concat(1)
hid1 = nn.Sequential()
hid1:add(nn.Linear(2, hs))
hid1:add(nn.ReLU())
block1:add(hid1)
block1:add(nn.Identity())

block2 = nn.Concat(1)
hid2 = nn.Sequential()
hid2:add(nn.Linear(hs+2, hs))
hid2:add(nn.ReLU())
block2:add(hid2)
block2:add(nn.Narrow(1, 6, 2))

mlp = nn.Sequential()
mlp:add(block1)
mlp:add(block2)
mlp:add(nn.Linear(hs+2, 3))
mlp:add(nn.Sigmoid())

a = torch.randn(2)
b = block1:forward(a)
c = block2:forward(b)

print(a)
print(b)
print(c)
print(mlp:forward(a))
